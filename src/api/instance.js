import axios from 'axios'

const instance = axios.create({
    baseURL: 'http://localhost:3000',
    timeOut: 5000
});

const createAPI = (url,method,config = {}) => {
    return instance({
        url,
        method,
        ...config
    })
}

instance.interceptors.request.use(data => {
    return data;
}, err => {
    return err
});

instance.interceptors.response.use(data => {
    return data;
}, err => {
    return err;
});

const discover = {
    getBanner: config => createAPI('/banner','get',config),
    getRecommendList: config => createAPI('/personalized?limit=10','get',config),
    getPrivateContent: config => createAPI('/personalized/privatecontent','get',config),
    getTopSong: config => createAPI('/top/song','get',config),
    getRecmmendMV: config => createAPI('/personalized/mv','get',config)
}

const songList = {
    getCategory: config => createAPI('/playlist/catlist','get',config),
    getSongList: config => createAPI('/top/playlist','get',config)
}

const topList = {
    getTopList: config => createAPI('/toplist','get',config),
    getRankDetail: config => createAPI('/playlist/detail','get',config),
    getTopArtist: config => createAPI('/toplist/artist','get',config)
}

const singerList = {
    getSingerList: config => createAPI('/artist/list','get',config)
}

const newMusic = {
    getNewMusic: config => createAPI('/top/song','get',config)
}

const musicPlay = {
    getComments: config => createAPI('/comment/hot','get',config),
    getSongText: config => createAPI('/lyric','get',config)
}

const songListPage = {
    getSongListDetails: config => createAPI('/playlist/detail','get',config),
    getSongDetails: config => createAPI('/song/detail','get',config),
    getSongListComments: config => createAPI('/comment/playlist','get',config)
}

export {
    discover,
    songList,
    topList,
    singerList,
    newMusic,
    musicPlay,
    songListPage
}