import Vue from 'vue'
import VueRouter from 'vue-router'
const MusicDisplay = ()=>import('@/views/musicDisplay')
const MusicList = ()=>import('@/views/musicDisplay/musicList')
const SongList = ()=>import('@/views/musicDisplay/songList')
const TopList = ()=>import('@/views/musicDisplay/topList')
const Singer = ()=>import('@/views/musicDisplay/singer')
const NewMusic = ()=>import('@/views/musicDisplay/newMusic')
const SongListPage = ()=>import('@/views/songList')


Vue.use(VueRouter)
  const routes = [
    {
      path:'/',
      redirect:'/musicDisplay/musicList'
    },
    {
      path:'/musicDisplay',
      component:MusicDisplay,
      children:[
        {
          path:'/musicDisplay',
          redirect:'musicList'
        },
        {
          path:'/musicDisplay/musicList',
          component:MusicList
        },
        {
          path:'/musicDisplay/songList',
          component:SongList
        },
        {
          path:'/musicDisplay/topList',
          component:TopList
        },
        {
          path:'/musicDisplay/singer',
          component:Singer
        },
        {
          path:'/musicDisplay/newMusic',
          component:NewMusic
        }
      ]
    },
    {
      path:'/songList/:id',
      component:SongListPage
    }
  ]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
