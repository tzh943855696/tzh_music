export const mixin = {
    computed: {
        playAmount() {
          return function(amount) {
              if(amount<10000) return amount
              return parseInt( amount/10000 ) + '万'
          }
        },
        getTime() {
            return function(time) {
                let tm = new Date(time)
                let min = tm.getMinutes()
                let second = tm.getSeconds()
                if(min<10) min = '0' + min
                if(second<10) second = '0' + second
                return `${min}:${second}`
            }
        }
    },
    methods: {
        addMusic(info) {
            this.$store.commit('pushSong',info)
        },
        jumpList(id) {
            this.$router.push({
                path: `/songList/${id}`
            })
        }
    }
}