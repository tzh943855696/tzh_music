import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      showDetail: false,
      isplaying: false,
      currentTime: 0,
      lyricList: [],
      lyricIndex: 0 //记录当前播放的是哪一个
  },
  getters: {
    getCurrentTime(state) {
      let min = parseInt(state.currentTime / 60) < 10 ? '0'+parseInt(state.currentTime / 60) : parseInt(state.currentTime / 60)
      let second = state.currentTime % 60 < 10 ? '0'+parseInt(state.currentTime % 60) : parseInt(state.currentTime % 60)
      return `${min}:${second}` 
    },
    getCurrent(state) {
      return state.currentTime
    },
    getCurrentSong(state) {
      return state.lyricList[state.lyricIndex]
    },
    getPlayStatus(state) {
      return state.isplaying
    },
    //有下一首
    hasNext(state) {
      if(state.lyricIndex === state.lyricList.length - 1) return false
      return true
    },
    //有上一首
    hasPre(state) {
      if(state.lyricIndex === 0) return false
      return true
    },
    getSongIndex(state) {
      return state.lyricIndex
    }
  },
  mutations: {
    toggleDisplay(state) {
        state.showDetail = !state.showDetail
    },
    changeCurrentTime(state,time) {
      state.currentTime = time
    },
    pushSong(state,config) {
      state.isplaying = true
      state.lyricList.forEach((item,index) => {
        if(item.id === config.id) {
          state.lyricList.splice(index,1)
        }
      })
      state.lyricList.unshift(config)
      state.lyricIndex = 0
    },
    togglePlaying(state) {
      state.isplaying = !state.isplaying
    },
    //如果到最后一首不做改变，如果不是则切换下首
    next(state) {
      if(state.lyricIndex === state.lyricList.length - 1) return
      state.lyricIndex++
    },
    //上一首
    pre(state) {
      state.lyricIndex--
    }
  },
  actions: {
  },
  modules: {
  }
})
